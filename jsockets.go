package jsockets

import (
	"code.google.com/p/go.net/websocket"
	"log"
)

// >>>type JsonWebsocket struct<<<
// this needs a place to store the websocket.Config (including the URL)
// and a flag or some mechanism to control if/when a connection
// is reattempted before giving up, or after a connection failure
// maxAttemptWaitSeconds  (quitAfter)
// maxAttemptTimes  (resets after successful connection)
// minRetryWaitSeconds / maxRetryWaitSeconds

type JsonWebsocket struct {
	depth int
	done chan bool
	ws   *websocket.Conn
	fromWS chan map[string]interface{}    // fromWS
	toWS chan map[string]interface{}      // toWS
}

func NewJsonWebsocket(feedUrl string, depth int) (*JsonWebsocket, error) {
	var srcConfig *websocket.Config
	var err error 
    done := make(chan bool)

	if srcConfig, err = websocket.NewConfig(feedUrl, "http://localhost"); err != nil {
		//log.Fatalf(err.Error())
		return nil, err
	}    
    
    log.Printf("Dialing: [%s]",feedUrl)	
	ws, err := websocket.DialConfig(srcConfig)
	if err != nil {
		//log.Printf("No connection to: [%s]",feedUrl)
		return nil, err
	}

	return &JsonWebsocket{depth: depth, done: done, ws: ws}, nil
}

func PromoteJsonWebsocket(ws *websocket.Conn, depth int) (*JsonWebsocket, error) {
    //done := make(chan bool)
	return &JsonWebsocket{ws: ws, depth: depth, done: make(chan bool)}, nil
}

// Get websocket connection.
func (self *JsonWebsocket) Conn() *websocket.Conn {
	return self.ws
}

// Get websocket connection.
func (self *JsonWebsocket) Close() {
	self.ws.Close()
}

// Get Inbound channel(from JsonWebsocket)
func (self *JsonWebsocket) Inbound() <-chan map[string]interface{} {
	return (<-chan map[string]interface{})(self.fromWS)
}

// Get Outbound channel(to JsonWebsocket)
func (self *JsonWebsocket) Outbound() chan<- map[string]interface{} {
	return (chan<- map[string]interface{})(self.toWS)
}

// Get done channel.
func (self *JsonWebsocket) Done() chan<-bool {
	return (chan<-bool)(self.done)
}

// Send JSON Msg on websocket
func (self *JsonWebsocket) SendJSON(msg map[string]interface{}) {
	websocket.JSON.Send(self.ws, msg)
}

// Assign or create Inbound and Outbound channels
func (self *JsonWebsocket) Link(fromChan chan map[string]interface{}, toChan chan map[string]interface{}) {	
    if self.fromWS == nil && fromChan == nil{       
	   self.fromWS = make(chan map[string]interface{}, self.depth)
    } else {
       self.fromWS = fromChan
    }
    
    if self.toWS == nil && toChan == nil{       
	   self.toWS = make(chan map[string]interface{}, self.depth)
    } else {
       self.toWS = toChan
    }    
}

// Begin the flow of messages from/to the websocket
func (self *JsonWebsocket) Convey() {
	go self.flowOutbound()
	self.flowInbound()
}

// Stop the flow of messages from/to the websocket
func (self *JsonWebsocket) Halt() {
	self.done <- true
}

// Stop the flow of messages from/to the websocket
func (self *JsonWebsocket) Reconnect() {
	var err error 	   
    
	ws, err := websocket.DialConfig(self.ws.Config())	
	if err != nil {
		return 
	}
	self.ws = ws
}

// Restart the flow of messages from/to the websocket
func (self *JsonWebsocket) Restart() {
	self.Halt()
	self.ws.Close()
	self.Reconnect()
	go self.Convey()
}

// Listen write request via chanel
func (self *JsonWebsocket) flowOutbound() {
	//log.Printf("Starting outflow")
	for {
		select {
			// send message to the client
			case msg := <-self.toWS:
				//log.Println("Send:", msg)
				websocket.JSON.Send(self.ws, msg)

			// receive done request
			case <-self.done:
				//self.server.RemoveClient() <- self
				// NEED CODE HERE
				self.done <- true // second message, for flowInbound method
				return
		}
	}
}

// Listen read request via chanel
func (self *JsonWebsocket) flowInbound() {
	//log.Printf("Starting Inflow of [%s]", self.ws.Config().Location)

Outerloop:
	for {
		select {
			// THIS NEEDS HELP, BLOCKS WAITING IN DEFAULT, 
			// MISSES self.done  see advanced patterns
			// receive done request
			case <-self.done:
				break Outerloop			
			// read data from websocket connection
			default:
				var msg map[string]interface{}
				err := websocket.JSON.Receive(self.ws, &msg)
				if err != nil {
					//log.Printf("error: non-JSON message")
					websocket.Message.Send(self.ws, `{"error":"non-JSON message"}`)
					//break Outerloop
				} else {
					self.fromWS <- msg
				}
		}
	}
	self.done <- true // second message, for flowOutbound method
}
